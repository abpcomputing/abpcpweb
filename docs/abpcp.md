# Accelerator and Beam Physics Computing Project

## Mandate

The Accelerator and Beam Physics Computing project:

 - coordinates the development and maintenance of beam physics software tools under the responsibility of BE-ABP
 - reports to the ABP-SLM, reviewing at least yearly objectives and resource allocation
 - defines the strategy for both the short- and long-term development, ensuring that the software evolves in response to the needs of CERN accelerators and projects
 - aims at optimizing resource utilization through effective coordination and harmonization of software development efforts across different areas
 - ensures that software tools are seamlessly integrated, that the chosen design and technologies are compatible with users’ and developers’ skills, and that code, testing, and documentation adhere to high quality standards
 - fosters knowledge transfer across development areas and ensures long-term retention of essential know-how
 - pursues synergy and collaboration with other ATS groups on beam-physics related software development and exploitation of computing resources
 - identifies the needs for hardware resources for beam physics computing and coordinates the exploitation of the available computing facilities, liaising with the IT department and external computing centers


## Organization

**Project leader:** Giovanni Iadarola

**Deputy project leader:** Riccardo De Maria

**Section coordinators:**

- Giulia Bellodi (HSL)
- Xavier Buffat (CEI)
- Laurent Deniau (LNO)
- Sofia Kostoglou (INC)
- Andrea Latina (LAF)
- Frederik Van Der Veken (NDC)


