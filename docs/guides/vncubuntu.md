# Accessing your Linux computer from home

1. First, you need somewhat to have access to the CERN network. The suggested method is to use [sshuttle](../sshtunnel).

2. If you use Gnome on your CERN desktop computer, a very simple way to get access to your screen is described on [linuxhint.com](https://linuxhint.com/enable-remote-desktop-ubuntu-access-windows/). This is the easiest to setup and allows you to have control of your local X session.

3. Alternatively, a general tool is to use `xrdp` service which is probably available on any linux distribution. A possible guide is on [digitalocean.com](https://www.digitalocean.com/community/tutorials/how-to-enable-remote-desktop-protocol-using-xrdp-on-ubuntu-22-04). However, this seems to be a bit more complicated to setup as one may wish.

4. From your home computer, you can use any remote desktop client (for windows, linux, mac) to access your CERN machine.

### Typical known issues

- It might be that you then need to disable screen lock on your computer: this clearly is not particularly safe especially if you share your office with others.

- There might be both gnome-screen-sharing service and xrdp running on your CERN computer: the latter, will prevent the first one to work! 


